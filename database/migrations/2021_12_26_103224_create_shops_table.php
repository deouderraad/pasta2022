<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('order_num');

            $table->string('fname');
            $table->string('lname');
            $table->string('email');
            $table->string('phone');

            $table->integer('pickup_slot');

            $table->string('payment_id')->nullable();
            $table->string('payment_status')->default(\Mollie\Api\Types\PaymentStatus::STATUS_OPEN);

            $table->dateTime('confirmed_at')->nullable();

            $table->timestamps();
        });

        Schema::create('order_lines', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('order_id');

            $table->string('sku');
            $table->string('name');
            $table->text('description')->nullable();

            $table->integer('price');
            $table->integer('amount');
            $table->json('data')->default('[]');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
