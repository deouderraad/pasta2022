<?php

namespace App\Data\Shop;

class EuroPrice
{
    private $cents;

    public function __construct(int $cents)
    {
        $this->cents = $cents;
    }

    public static function create(int $cents)
    {
        return new static($cents);
    }

    public function __toString()
    {
        return '&euro;' . number_format($this->cents / 100, 0, ',', '');
    }
}
