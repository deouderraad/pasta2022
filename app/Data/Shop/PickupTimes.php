<?php

namespace App\Data\Shop;

use App\Models\Order;

class PickupTimes
{
    private static $instance;

    private $slots = [

    ];

    private $max = 40;

    private function __construct()
    {
        $slots = range(9, 14);

        foreach ($slots as $num) {
            $this->slots[$num] = ['label'=>$this->slotLabel($num), 'available'=>$this->max];
        }

        Order::where(['payment_status'=>'paid'])->get()->map(function ($order) {
            $this->slots[$order->pickup_slot]['available'] -= 1;
        });
    }

    public static function get():self
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function isAvailableSlot(int $num)
    {
        if (!array_key_exists($num, $this->slots)) {
            return false;
        }

        return $this->slots[$num]['available'] > 0;
    }

    public function slotLabel(int $num, string $join = '-')
    {
        $starts = substr("0{$num}", -2) . ':00';
        $end = $num + 1;
        $ends = substr("0{$end}", -2) . ':00';

        return "{$starts} {$join} {$ends}";
    }

    public function getSlots(): array
    {
        return $this->slots;
    }
}
