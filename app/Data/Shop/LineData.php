<?php

namespace App\Data\Shop;

class LineData
{
    private $sku;
    private $name;
    private $amount;
    private $price;
    private $description;
    private $data;

    public function __construct(
        string $sku,
        string $name,
        int $amount,
        int $price,
        ?string $description = null,
        array $data = []
    ) {
        $this->sku = $sku;
        $this->name = $name;
        $this->amount = $amount;
        $this->price = $price;
        $this->description = $description;
        $this->data = $data;
    }

    public function toArray(): array
    {
        return array_filter([
            'sku' => $this->sku,
            'name' => $this->name,
            'amount' => $this->amount,
            'price' => $this->price,
            'description' => $this->description,
            'data' => $this->data,
        ]);
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function hasDescription(): bool
    {
        return !empty($this->getDescription());
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getTotalPrice(): int
    {
        return $this->getPrice() * $this->getAmount();
    }

    public static function sauce(string $sku, int $amount)
    {
        $labels = Labels::sauce($sku);

        return new static(
            $sku, "{$labels['name']} Saus", $amount, 1500
        );
    }

    public static function wine(string $sku, int $amount)
    {
        if ($sku === 'w_red') {
            return new static('w_red', 'Rode wijn: Un momento de Tempranillo (2020)', $amount, 1200);
        }

        if ($sku === 'w_white') {
            return new static('w_white', 'Witte wijn: Un momento de Verdejo (2020)', $amount, 1200);
        }
    }

    public static function pack(string $sku, array $desserts = [])
    {
        $labels = Labels::sauce($sku);

        return new static(
            $sku, "{$labels['name']} Pakket", 1, 2800, static::desserts($desserts), static::getDessertCount($desserts)
        );
    }

    private static function desserts(array $desserts)
    {
        $count = static::getDessertCount($desserts);

        $labels = [
            'tira' => 'Tiramisu',
            'choco' => 'Chocomousse',
            'panna' => 'Panna cotta',
        ];

        $out = [];
        foreach ($labels as $k => $label) {
            if (array_key_exists($k, $count)) {
                $out[$k] = "{$count[$k]} x {$label}";
            }
        }

        return implode(', ', $out);
    }

    private static function getDessertCount(array $desserts): array
    {
        $count = [
            'choco' => 0,
            'tira' => 0,
            'panna' => 0,
        ];
        foreach ($desserts as $d) {
            $count[$d] += 1;
        }

        return array_filter($count);
    }
}
