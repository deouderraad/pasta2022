<?php

namespace App\Data\Shop;

class Cart
{
    private $sauces = [];
    private $packs = [];
    private $wines = [];
    private $desserts = [];

    public function __construct(array $sauces = [], array $packs = [], array $wines = [], array $desserts = [])
    {
        $this->sauces = $sauces;
        $this->packs = $packs;
        $this->wines = $wines;
        $this->desserts = $desserts;
    }

    public static function current()
    {
        static $instance;
        if (is_null($instance)) {
            $instance = new static(
                session()->get('cart.sauces', []),
                session()->get('cart.packs', []),
                session()->get('cart.wines', []),
                session()->get('cart.desserts', [])
            );
        }

        return $instance;
    }

    public function setSauce(string $sku, int $amount)
    {
        $this->sauces[$sku] = $amount;
    }

    public function getSauce(string $sku): ?int
    {
        return $this->sauces[$sku] ?? null;
    }

    public function getSauces(bool $filter = true): array
    {
        return $filter ? array_filter($this->sauces) : $this->sauces;
    }

    public function setPack(string $sku, int $amount)
    {
        $this->packs[$sku] = $amount;
    }

    public function getPack(string $sku): ?int
    {
        return $this->packs[$sku] ?? null;
    }

    public function getPacks(bool $filter = true): array
    {
        return $filter ? array_filter($this->packs) : $this->packs;
    }

    public function setWine(string $sku, int $amount)
    {
        $this->wines[$sku] = $amount;
    }

    public function getWine(string $sku): ?int
    {
        return $this->wines[$sku] ?? null;
    }

    public function getWines(bool $filter = true): array
    {
        return $filter ? array_filter($this->wines) : $this->wines;
    }

    public function getDessertKeys(): array
    {
        $packs = $this->getPacks();
        $out = [];

        foreach ($packs as $sku => $count) {
            for ($i = 0; $i < $count; $i++) {
                $out[] = "{$sku}_{$i}";
            }
        }

        return $out;
    }

    public function getDesserts(bool $filter = true): array
    {
        return $filter ? array_filter($this->desserts) : $this->desserts;
    }

    public function setDesserts(string $key, array $desserts)
    {
        $this->desserts[$key] = array_filter($desserts);
    }

    public function getDessertsFor(string $key): array
    {
        $desserts = $this->getDesserts();

        return $desserts[$key] ?? [];
    }

    public function hasRequiredDesserts(): bool
    {
        foreach ($this->getDessertKeys() as $k) {
            $check = array_filter($this->getDessertsFor($k));
            if (count($check) < 4) {
                return false;
            }
        }

        return true;
    }

    public function save()
    {
        session()->put('cart', [
            'sauces' => $this->getSauces(),
            'packs' => $this->getPacks(),
            'wines' => $this->getWines(),
            'desserts' => $this->getDesserts(),
        ]);
    }

    public function clear()
    {
        session()->remove('cart');
    }
}
