<?php

namespace App\Data\Shop;

class Lines
{
    private $lines = [];

    public function __construct(array $lines = [])
    {
        array_map([$this, 'add'], $lines);
    }

    public function add(LineData $line)
    {
        $this->lines[] = $line;

        return $this;
    }

    public function toArray(): array
    {
        return array_map(function (LineData $line) {
            return $line->toArray();
        }, $this->lines);
    }

    public function sauce(string $sku, int $amount)
    {
        return $this->add(LineData::sauce($sku, $amount));
    }

    public function pack(string $sku, array $desserts)
    {
        return $this->add(LineData::pack($sku, $desserts));
    }

    public function wine(string $sku, int $amount)
    {
        return $this->add(LineData::wine($sku, $amount));
    }

    /**
     * @return \App\Data\Shop\LineData[]
     */
    public function getLines(): array
    {
        return $this->lines;
    }

    public function getTotalPrice(): int
    {
        return array_reduce($this->lines, function ($carry, LineData $item) {
            $carry += $item->getTotalPrice();

            return $carry;
        }, 0);
    }
}
