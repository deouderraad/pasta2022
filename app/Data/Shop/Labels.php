<?php

namespace App\Data\Shop;

class Labels
{
    public static function sauces()
    {
        return [
            'bolo' => ['name' => 'Bolognaise', 'description' => 'Spaghettisaus op basis van van varken/rundgehakt en groenten'],
            'veggie' => ['name' => 'Vegetarisch', 'description' => 'Spaghettisaus op basis van sojagehakt, tomatensaus en stukjes groenten'],
            'carbo' => ['name' => 'Carbonara', 'description' => 'Romige kaassaus met spekblokjes.'],
            'pesto' => ['name' => 'Pesto', 'description' => 'Vegetarische pesto saus met tomaten.'],
            'arrab' => ['name' => "All'arrabbiata", 'description' => 'Pittige tomatensaus, spekblokjes en Parmezaanse kaas.'],
        ];
    }

    public static function sauce(string $sku)
    {
        if (substr($sku, 0, 2) === 'p_' || substr($sku, 0, 2) === 's_') {
            $sku = substr($sku, 2);
        }
        $sauces = self::sauces();

        return $sauces[$sku] ?? null;
    }
}
