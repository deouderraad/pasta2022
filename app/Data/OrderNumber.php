<?php

namespace App\Data;

use App\Models\Order;
use Carbon\Carbon;
use DateTimeInterface;

class OrderNumber
{
    public static function next(?DateTimeInterface $date = null)
    {
        $date = $date ?? Carbon::now();
        $count = 1 + Order::whereDate('created_at', '=', $date->format('Y-m-d'))->count();

        return $date->format('Ymd') . '-' . substr("000{$count}", -3);
    }
}
