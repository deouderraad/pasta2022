<?php

namespace App\Data;

use Mollie\Api\MollieApiClient;

class Mollie
{
    private static $client;

    public static function client():MollieApiClient
    {
        if (is_null(self::$client)) {
            self::$client = new MollieApiClient();
            self::$client->setApiKey($_ENV['MOLLIE_API_KEY']);
        }

        return self::$client;
    }
}
