<?php

namespace App\Data\Action;

use App\Data\Shop\Cart;
use App\Data\Shop\Lines;

class CartToLines
{
    public function execute(Cart $cart)
    {
        $lines = new Lines();
        foreach ($cart->getPacks() as $sku => $num) {
            for ($a = 0; $a < $num; $a++) {
                $lines->pack($sku, $cart->getDessertsFor("{$sku}_{$a}"));
            }
        }

        foreach ($cart->getSauces() as $sku => $amount) {
            $lines->sauce($sku, $amount);
        }

        foreach ($cart->getWines() as $sku => $amount) {
            $lines->wine($sku, $amount);
        }

        return $lines;
    }
}
