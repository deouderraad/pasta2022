<?php

namespace App\Console\Commands;

use App\Mail\PickupInfo;
use App\Mail\PickupReminder;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class PickupMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:pickup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send pickup mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::where([
            'payment_status' => 'paid',
            'pickup_mail' => null,
        ])->limit(10);
        echo $orders->count();

        $orders->get()->map(function (Order $order) {
            $mail = new PickupReminder($order);
            $mail->subject = 'Pasta in uw kot: afhaling';
            Mail::to($order->email)->send($mail);
            $order->pickup_mail = Carbon::now();
            $order->save();
        });

        return 0;
    }
}
