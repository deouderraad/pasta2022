<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class InstallOr extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'or:install';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->installWine();
        $this->installSauce();
        $this->installPacks();

        return 0;
    }

    private function installWine()
    {
        $wines = [
            ['sku' => 'w_white', 'name' => 'Un momento de Verdejo (Wit, 2020)', 'price' => 1200],
            ['sku' => 'w_red', 'name' => 'Un momento de Tempranillo (Rood, 2020)', 'price' => 1200],
        ];

        return $this->installProductsFromArray($wines);
    }

    private function installSauce()
    {
        $sauces = [
            ['sku' => 's_bolo', 'name' => 'Bolognaise (saus)', 'description'=>'Spaghettisaus op basis van van varken/rundgehakt en groenten.', 'price' => 1500],
            ['sku' => 's_veggie', 'name' => 'Vegetarisch (saus)', 'description'=>'Spaghettisaus op basis van sojagehakt, tomatensaus en stukjes groenten.', 'price' => 1500],
            ['sku' => 's_carbo', 'name' => 'Carbonara (saus)', 'description'=>'Romige kaassaus met spekblokjes.', 'price' => 1500],
            ['sku' => 's_pesto', 'name' => 'Pesto (saus)', 'description'=>'Vegetarische pesto saus met tomaten', 'price' => 1500],
            ['sku' => 's_arrab', 'name' => "All'arrabbiata (saus)", 'description'=>'Pittige tomatensaus, spekblokjes en Parmezaanse kaas.', 'price' => 1500],
        ];

        return $this->installProductsFromArray($sauces);
    }

    private function installPacks()
    {
        $packs = [
            ['sku' => 'p_bolo', 'name' => 'Bolognaise (pakket)', 'price' => 2800],
            ['sku' => 'p_veggie', 'name' => 'Vegetarisch (pakket)', 'price' => 2800],
            ['sku' => 'p_carbo', 'name' => 'Carbonara (pakket)', 'price' => 2800],
            ['sku' => 'p_pesto', 'name' => 'Pesto (pakket)', 'price' => 2800],
            ['sku' => 'p_arrab', 'name' => "All'arrabbiata (pakket)", 'price' => 2800],
        ];

        $packs = $this->installProductsFromArray($packs);
        /*
              $packs->map(function (Product $pack) {
                  return $this->installDesserts($pack);
              });
*/
        return $packs;
    }

    private function installDesserts(Product $pack)
    {
        $settings = [
            'options' => [
                'tira' => 'Tiramisu',
                'chomo' => 'Chocomousse',
                'paco' => 'Panna cotta',
            ],
        ];

        $desserts = [
            ['option_id' => 'dessert_1', 'name' => 'Dessert 1', 'type' => 'select', 'settings' => $settings],
            ['option_id' => 'dessert_2', 'name' => 'Dessert 2', 'type' => 'select', 'settings' => $settings],
            ['option_id' => 'dessert_3', 'name' => 'Dessert 3', 'type' => 'select', 'settings' => $settings],
            ['option_id' => 'dessert_4', 'name' => 'Dessert 4', 'type' => 'select', 'settings' => $settings],
        ];

        collect($desserts)->map(function ($dessert) use ($pack) {
            $option = $pack->options()->where('option_id', $dessert['option_id'])->first();

            if (is_null($option)) {
                $option = $pack->options()->create($dessert);
            }

            $option->name = $dessert['name'];
            $option->type = $dessert['type'];
            $option->settings = $dessert['settings'];
            $option->save();
        });

        return $pack;
    }

    private function installProductsFromArray(array $infos)
    {
        foreach ($infos as $info) {
            $product = Product::fromSku($info['sku']);
            if (is_null($product)) {
                $product = Product::create($info);
            }
            $product->setAttribute('name', $info['name']);
            $product->setAttribute('price', $info['price']);
            $product->save();
        }

        return collect($infos)->map(function ($info) {
            $product = Product::fromSku($info['sku']);
            if (is_null($product)) {
                $product = Product::create($info);
            }
            $product->setAttribute('name', $info['name']);
            $product->setAttribute('price', $info['price']);
            $product->save();

            return $product;
        });
    }
}
