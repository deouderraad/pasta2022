<?php

namespace App\Http\Controllers;

use App\Data\Action\CartToLines;
use App\Data\Shop\Cart;
use App\Data\Shop\LineData;
use App\Data\Shop\PickupTimes;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController
{
    public function step_1(Request $request)
    {
        if ($request->get('clear') === '1') {
            Cart::current()->clear();

            return redirect(route('order.step_1'));
        }

        return view('shop.order.step_1');
    }

    public function handle_step_1(Request $request)
    {
        $cart = Cart::current();

        $skus = ['bolo', 'veggie', 'carbo', 'pesto', 'arrab'];

        foreach ($skus as $sku) {
            $cart->setSauce("s_{$sku}", (int) $_POST["s_{$sku}"]);
            $cart->setPack("p_{$sku}", (int) $_POST["p_{$sku}"]);
        }

        $cart->save();

        if (empty($cart->getPacks()) && empty($cart->getSauces())) {
            return redirect(route('order.step_1'));
        }

        return redirect(route('order.step_2'));
    }

    public function step_2(Request $request)
    {
        $cart = Cart::current();
        if (empty($cart->getPacks()) && empty($cart->getSauces())) {
            return redirect(route('order.step_1'));
        }

        return view('shop.order.step_2', ['cart' => $cart]);
    }

    public function handle_step_2(Request $request)
    {
        $cart = Cart::current();
        $cart->setWine('w_white', (int) $_POST['w_white']);
        $cart->setWine('w_red', (int) $_POST['w_red']);
        $cart->save();

        $validation = [];
        foreach ($cart->getDessertKeys() as $k) {
            $validation["dessert.{$k}"] = [function ($attribute, $value, $fail) {
                if (count(array_filter($value)) < 4) {
                    $fail('Gelieve 4 desserts te kiezen.');
                }
            }];
            $cart->setDesserts($k, $_POST['dessert'][$k]);
        }
        $cart->save();
        $request->validate($validation);

        return redirect(route('order.confirm'));
    }

    public function confirm(Request $request)
    {
        $cart = Cart::current();
        if (empty($cart->getPacks()) && empty($cart->getSauces())) {
            return redirect(route('order.step_1'));
        }
        if (!$cart->hasRequiredDesserts()) {
            return redirect(route('order.step_2'));
        }
        $lines = (new CartToLines())->execute(Cart::current());

        return view('shop.order.confirm', ['lines' => $lines]);
    }

    public function handle_confirm(Request $request)
    {
        $cart = Cart::current();
        if (empty($cart->getPacks()) && empty($cart->getSauces())) {
            return redirect(route('order.step_1'));
        }

        if (!$cart->hasRequiredDesserts()) {
            return redirect(route('order.step_2'));
        }

        $request->validate([
            'fname' => ['required'],
            'lname' => ['required'],
            'email' => ['required', 'email'],
            'phone' => ['required'],
            'av' => ['required'],
            'pickup_slot' => [function ($attribute, $value, $fail) {
                if (!PickupTimes::get()->isAvailableSlot((int) $value)) {
                    $fail('Kies een tijdsstip waar nog plaats is.');
                }
            }],
        ], [
            'fname.required' => 'Verplicht veld',
            'lname.required' => 'Verplicht veld',
            'email.required' => 'Verplicht veld',
            'email.email' => 'Ongeldig adres',
            'phone.required' => 'Verplicht veld',
            'av.required' => 'U moet akkoord gaan met de voorwaarden om te kunnen bestellen',
        ]);

        $data = $request->only(['fname', 'lname', 'email', 'phone', 'pickup_slot']);

        $order = Order::create($data);

        $lines = (new CartToLines())->execute(Cart::current());
        array_map(function (LineData $line) use ($order) {
            $order->lines()->create($line->toArray());
        }, $lines->getLines());

        session()->remove('cart');
        session()->put('order_id', $order->getKey());

        return redirect($order->payment()->getCheckoutUrl());
    }

    public function done(Request $request, $id)
    {
        /**
         * @var  Order
         */
        $order = Order::where(['id' => $id])->first();

        if (!$order) {
            return $this->clear();
        }

        if ($request->get('retry') === '1' && !$order->payment()->isPaid()) {
            return \redirect($order->refreshPayment()->getCheckoutUrl());
        }

        $order->refreshPaymentStatus();

        if ($order->payment()->isPaid()) {
            return view('shop.order.paid', ['order'=>$order]);
        }

        return view('shop.order.not_paid', ['order'=>$order]);
    }

    private function clear()
    {
        session()->remove('order_id');
        session()->remove('cart');

        return redirect(route('home'));
    }
}
