<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Psr\Log\LogLevel;

class MollieController
{
    public function receive(Request $request, $id)
    {
        Log::write(LogLevel::INFO, 'mollie received', ['id'=>$id, 'body'=>$request->getContent()]);
    }
}
