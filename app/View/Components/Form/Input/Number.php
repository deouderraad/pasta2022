<?php

namespace App\View\Components\Form\Input;

class Number extends Input
{
    public function __construct(string $id, ?int $value = null, ?string $name = null, ?string $label = null)
    {
        parent::__construct($id, 'number', $value, $name, $label);
        $this->class = [
            'border', 'pl-2', 'w-full',
        ];
    }
}
