<?php

namespace App\View\Components\Form\Input;

use Illuminate\View\Component;

class Button extends Component
{
    public $type;
    public $style;

    public function __construct(string $type = 'button', string $style = 'ok')
    {
        $this->type = $type;
        $this->style = $style;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.input.button');
    }
}
