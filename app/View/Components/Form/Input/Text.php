<?php

namespace App\View\Components\Form\Input;

class Text extends Input
{
    public function __construct(string $id, ?int $value = null, ?string $name = null, ?string $label = null)
    {
        parent::__construct($id, 'text', $value, $name, $label);
        $this->class = [
            'border', 'px-2', 'w-full',
        ];
    }
}
