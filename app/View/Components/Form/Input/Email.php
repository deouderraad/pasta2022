<?php

namespace App\View\Components\Form\Input;

class Email extends Input
{
    public function __construct(string $id, ?int $value = null, ?string $name = null, ?string $label = null)
    {
        parent::__construct($id, 'email', $value, $name, $label);
        $this->class = [
            'border', 'px-2', 'w-full',
        ];
    }
}
