<?php

namespace App\View\Components\Form\Input;

use Illuminate\View\Component;

class Input extends Component
{
    public $type;
    public $id;
    public $value;
    public $name;
    public $label;
    public $class;

    public function __construct(string $id, string $type = 'text', ?int $value = null, ?string $name = null, ?string $label = null)
    {
        $this->id = $id;
        $this->type = $type;
        $this->value = $value;
        $this->name = $name ?? $id;
        $this->label = $label;

        $this->class = [];
    }

    public function render()
    {
        $old = session("_old_input.{$this->id}");

        if (!empty($old)) {
            $this->value = $old;
        }

        return view('components.form.input.input');
    }
}
