<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Title extends Component
{
    public function render()
    {
        return view('components.title');
    }
}
