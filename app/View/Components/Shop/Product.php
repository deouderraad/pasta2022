<?php

namespace App\View\Components\Shop;

use Illuminate\View\Component;

class Product extends Component
{
    public $sku;
    public $title;

    public function __construct(string $sku, string $title)
    {
        $this->sku = $sku;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.shop.product');
    }
}
