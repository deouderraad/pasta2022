<?php

namespace App\View\Components\Shop;

use App\Data\Shop\Labels;
use Illuminate\View\Component;

class Desserts extends Component
{
    public $sku;
    public $num;

    public function __construct(string $sku, string $num)
    {
        $this->sku = $sku;
        $this->num = $num;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $title = Labels::sauce($this->sku)['name'];

        return view('components.shop.desserts', [
            'title'=>'Pakket ' . $title . ' (' . ($this->num + 1) . ')',
            'pack'=> $this->sku . '_' . $this->num,
        ]);
    }
}
