<?php

namespace App\Models;

use App\Models\Behaviour\UuidIdentified;
use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * @method static static create(array $args)
 */
abstract class Model extends BaseModel
{
    use UuidIdentified;

    public function __construct(array $attributes = [])
    {
        $this->casts['id'] = 'string';

        parent::__construct($attributes);
    }
}
