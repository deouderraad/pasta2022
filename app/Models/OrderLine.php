<?php

namespace App\Models;

class OrderLine extends Model
{
    protected $attributes = [
        'data' => '[]',
    ];

    protected $guarded = ['id', 'created_at', 'updated_at'];
    public $casts = [
        'data' => 'array',
    ];

    public function usesTimestamps()
    {
        return false;
    }

    public function price(): int
    {
        return $this->amount * $this->price;
    }
}
