<?php

namespace App\Models\Behaviour;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait UuidIdentified
{
    public static function bootUuidIdentified()
    {
        parent::creating(function (Model $model) {
            $model->setIncrementing(false);
            $model->setKeyType('string');
            $model->casts['id'] = 'string';
            $model->setAttribute($model->getKeyName(), Str::uuid()->toString());
        });
    }
}
