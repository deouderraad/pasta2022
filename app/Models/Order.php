<?php

namespace App\Models;

use App\Data\Mollie;
use App\Data\OrderNumber;
use App\Data\Shop\LineData;
use App\Data\Shop\Lines;
use App\Data\Shop\PickupTimes;
use App\Mail\OrderReceived;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Mail;
use Mollie\Api\Resources\Payment;

class Order extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    private $orderlines;

    public function lines(): HasMany
    {
        return $this->hasMany(OrderLine::class, 'order_id', 'id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function (self $order) {
            $order->order_num = OrderNumber::next();
        });
    }

    public function payment(): Payment
    {
        if (is_null($this->payment_id)) {
            return $this->refreshPayment();
        }

        return Mollie::client()->payments->get($this->payment_id);
    }

    public function refreshPayment(): Payment
    {
        $payment = Mollie::client()->payments->create([
            'amount' => [
                'currency' => 'EUR',
                'value' => number_format($this->getOrderLines()->getTotalPrice() / 100, 2, '.', ','),
            ],
            'description' => "Pasta in uw kot bestelling {$this->order_num}",
            'redirectUrl' => route('done', ['id' => $this->id]),
            'webhookUrl' => route('mollie.receive', ['id' => $this->id]),
        ]);
        $this->payment_id = $payment->id;
        $this->save();

        return $payment;
    }

    public function getOrderLines(): Lines
    {
        if (is_null($this->orderlines)) {
            $this->orderlines = new Lines();
            $this->lines()->get()->map(function (OrderLine $line) {
                $this->orderlines->add(
                    new LineData(
                        $line->sku,
                        $line->name,
                        $line->amount,
                        $line->price,
                        $line->description,
                        $line->data
                    )
                );
            });
        }

        return $this->orderlines;
    }

    public function refreshPaymentStatus(bool $withMails = true)
    {

        if ($this->payment()->status != $this->payment_status) {
            $this->payment_status = $this->payment()->status;
            $this->save();
        }

        if ($withMails) {
            $this->sendMails();
        }

        return $this;
    }

    public function pickupLabel($join = '-')
    {
        return PickupTimes::get()->slotLabel($this->pickup_slot, $join);
    }

    private function sendMails()
    {
        if ($this->payment_status === 'paid' && is_null($this->confirmed_at)) {
            $mail = new OrderReceived($this);
            $mail->subject = 'Pasta in uw kot: uw bestelling';
            Mail::to($this->email)->send($mail);
            $this->confirmed_at = Carbon::now();
            $this->save();
        }
    }
}
