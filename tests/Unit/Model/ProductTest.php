<?php

namespace Tests\Unit\Model;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_has_sku_title_and_description()
    {
        $data = [
            'name' => 'My test product',
            'description' => 'My product description',
            'sku' => 'my-sku',
        ];
        $product = Product::create($data);
        $this->assertEquals($data['name'], $product->name);
        $this->assertEquals($data['description'], $product->description);
        $this->assertEquals($data['sku'], $product->sku);
        $this->assertNull($product->price);
    }

    public function test_it_can_have_price()
    {
        $data = [
            'name' => 'My test product',
            'sku' => 'my-sku',
            'price' => 1000,
        ];
        $product = Product::create($data);
        $this->assertEquals($data['name'], $product->name);
        $this->assertEquals($data['sku'], $product->sku);
        $this->assertEquals($data['price'], $product->price);
    }

    public function test_it_can_be_retrieved_by_sku()
    {
        $product = Product::create([
            'name' => 'My test product',
            'sku' => 'my-sku',
        ]);

        $this->assertEquals($product->getKey(), Product::fromSku('my-sku')->getKey());
    }

    public function test_it_returns_null_for_invalid_sku()
    {
        $this->assertNull(Product::fromSku('non-existing'));
    }
}
