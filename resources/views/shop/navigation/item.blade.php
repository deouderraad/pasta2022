<?php
/**
 * @var $num
 * @var $label
 * @var $isActive
 */
?>

<li @class([

        'pb-2',
        'text-lg',
        'md:text-center',
        'border-gray-400',
        'md:border-pink-400'=>$active,
        'border-b-2',
        'hidden'=>!$active,
        'md:block'

])>
    <span class="hidden">{{ $num }}</span>
    <span>{{ $label }}</span>
</li>
