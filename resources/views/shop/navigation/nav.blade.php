<?php
$active = $active ?? 1;
$items = [
    ['num' => '1', 'label' => 'Pakketten & Sauzen'],
    ['num' => '2', 'label' => 'Dranken & Desserts'],
    ['num'=>'3', 'label' => 'Afrekenen']
];
?>
<nav>
    <ul class="grid md:grid-cols-3 gap-1 mb-2">
        @foreach($items as $item)
            @include('shop.navigation.item', ['num'=>$item['num'], 'label'=>$item['label'], 'active'=>($item['num']== $active)])
        @endforeach
    </ul>
</nav>
