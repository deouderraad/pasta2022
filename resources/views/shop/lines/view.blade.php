<?php
/**
 * @var $lines \App\Data\Shop\Lines
 * @var $line \App\Data\Shop\LineData
 */
?>
<table class="w-full border-l border-x my-2">
    @foreach($lines->getLines() as $line)
        <?php
        /**
         * @var $line \App\Data\Shop\LineData
         */
        ?>
        <tbody>
        <tr class="border-t">
            <td class="align-top p-2">{{ $line->getAmount() }}</td>
            <td class="align-top p-2">
                {{ $line->getName() }}
                @if($line->hasDescription())
                    <span class="block">{{ $line->getDescription() }}</span>
                @endif
            </td>
            <td class="align-top p-2 text-center">{!!  \App\Data\Shop\EuroPrice::create($line->getPrice())  !!}</td>
            <td class="align-top p-2 text-center">{!!  \App\Data\Shop\EuroPrice::create($line->getTotalPrice())  !!}</td>
        </tr>
        </tbody>
    @endforeach
    <tfoot>
    <tr class="bg-gray-200">
        <td colspan="3" class="align-top p-2">Totaal</td>
        <td class="align-top p-2 text-center">{!!  \App\Data\Shop\EuroPrice::create($lines->getTotalPrice())  !!}</td>
    </tr>
    </tfoot>
</table>

