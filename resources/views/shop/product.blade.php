
<div class="flex">
    <div class="w-1/2">
        @include("form.num", ['label'=>'Pakket (&euro; 28)', 'id'=>"p_{$type}", 'value'=>$cart->getAmount("p_{$type}")])
    </div>
    <div class="w-1/2">
        @include("form.num", ['label'=>'Saus (&euro; 15)', 'id'=>"s_{$type}", 'value'=>$cart->getAmount("s_{$type}")])
    </div>
</div>
