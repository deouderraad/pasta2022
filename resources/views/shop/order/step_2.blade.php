@extends('page')
@section('main')
    @include('shop.navigation.nav', ['active'=>2])
    <form method="post">
        @csrf
        <main>
            <x-shop.wine></x-shop.wine>
            @include('shop.order.desserts')
        </main>
        <footer class="pt-4 flex justify-center md:justify-end">
            <x-form.input.button type="submit">Volgende &raquo;</x-form.input.button>
        </footer>
    </form>
@endsection
