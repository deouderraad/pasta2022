@extends('page')
@section('main')
    @include('shop.navigation.nav', ['active'=>3])
    <form method="post">
        @csrf

        <main>
            <x-title>Bestellen</x-title>
            <div>
                @include('shop.lines.view', ['lines'=>$lines])
            </div>
            <div>
                <x-subtitle>Persoonlijke gegevens</x-subtitle>
                <div class="grid md:grid-cols-2 gap-2">
                    <x-form.input.text id="fname" label="Voornaam"/>
                    <x-form.input.text id="lname" label="Achternaam"/>
                    <x-form.input.email id="email" label="E-mailadres"/>
                    <x-form.input.text id="phone" label="Telefoonnummer"/>
                </div>
            </div>
            <div>
                <x-subtitle>Afhaling</x-subtitle>
                <label for="pickup">
                    <?php $slots = \App\Data\Shop\PickupTimes::get()->getSlots();
                    $val = session('_old_input.pickup_slot');
                    ?>
                    <div class="flex justify-between">
                        <select id="pickup_slot" name="pickup_slot" class="border pl-2 border-gray-400 p-2">
                            <option value="">-- Maak uw keuze --</option>
                            @foreach($slots as $k=>$slot)
                                <option value="{{ $k }}"
                                        @if($slot['available'] === 0)
                                        disabled
                                        @endif
                                        @if($k == $val)
                                        selected
                                    @endif
                                >{{$slot['label']}}</option>
                            @endforeach
                        </select>

                        @error('pickup_slot')
                        <span class="bg-red-100 text-red-800 p-1 px-2 ml-1">{{  $message }}</span>
                        @enderror
                    </div>

                </label>
            </div>
            <div>
                <x-subtitle>Verkoopsvoorwaarden en privacy</x-subtitle>
                @error("av")
                <p class="bg-red-100 text-red-800 p-1 px-2 ml-1">{{  $message }}</p>
                @enderror
                <label for="av">
                    <input type="checkbox" name="av" id="av" value="ok">
                    Ik heb de <a href="{{ route('terms') }}" target="_blank"
                                 class="text-blue-400 hover:text-blue-600 text-sm">algemene voorwaarden</a> gelezen en ga hiermee akkoord
                </label>

            </div>
        </main>
        <footer class="pt-4 flex justify-center md:justify-end">
            <x-form.input.button type="submit">Bestellen</x-form.input.button>
        </footer>
    </form>
@endsection
