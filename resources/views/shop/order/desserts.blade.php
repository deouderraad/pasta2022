<?php
$cart = \App\Data\Shop\Cart::current();

?>
<x-title>Desserten</x-title>

@if(empty($cart->getPacks()))
    <p class="md-1">Desserts kunnen enkel gekozen worden wanneer je 1 of meerdere pakketten besteld.</p>
@endif

@foreach($cart->getPacks() as $sku=>$count)
    @for($a = 0; $a < $count; $a++)
        <x-shop.desserts sku="{{$sku}}" num="{{$a}}"></x-shop.desserts>
    @endfor
@endforeach

