@extends('page')
@section('main')
    @include('shop.navigation.nav', ['active'=>1])
    <form method="post">
        @csrf
        <main>

            <div>
                <p class="mb-1">Een pakket bevat alles wat je nodig hebt om een pastafeestje te bouwen: 1 liter saus, 500 gram ongekookte pasta, gemalen kaas en 4 desserts naar keuze.</p>
                <p class="mb-1">Je kan ook de saus apart kopen. De sauzen zijn verpakt in zakken van 1 liter en mogen ingevroren worden.</p>
            </div>

            <x-title>Sauzen</x-title>
            <x-shop.product sku="bolo" title="Bolognaise" >
                Spaghettisaus op basis van van varken/rundgehakt en groenten.
            </x-shop.product>

            <x-shop.product sku="veggie" title="Vegetarisch">
                Spaghettisaus op basis van sojagehakt, tomatensaus en stukjes groenten.
            </x-shop.product>

            <x-shop.product sku="carbo" title="Carbonara">
                Romige kaassaus met spekblokjes.
            </x-shop.product>

            <x-shop.product sku="pesto" title="Pesto">
                Vegetarische pesto saus met tomaten.
            </x-shop.product>

            <x-shop.product sku="arrab" title="All'arrabbiata">
                Pittige tomatensaus, spekblokjes en Parmezaanse kaas.
            </x-shop.product>

        </main>
        <footer class="pt-4 flex justify-center md:justify-end">
            <x-form.input.button type="submit">Volgende &raquo;</x-form.input.button>
        </footer>
    </form>
@endsection
