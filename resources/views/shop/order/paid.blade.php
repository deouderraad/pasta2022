@extends('page')
@section('main')
    <x-title>Bedankt</x-title>

    <div class="mb-2">
        <p>We hebben je bestelling en betaling goed ontvangen. Je ontvangt een bevestiging op
            <em><?= $order->email; ?></em></p>
        <p>Het bestelnummer is <em><?= $order->order_num; ?></em></p>
        <p>Dank je wel voor je steun!</p>
    </div>
    <div class="mb-2">
        <x-subtitle>Bestelling nummer {{ $order->order_num }}</x-subtitle>
        @include('shop.lines.view', ['lines'=>$order->getOrderLines()])
    </div>
    <div class="py-4 flex items-center justify-center">
        <a href="{{ route('order.step_1') }}?refresh=1" class="bg-blue-400 hover:bg-green-400 text-white py-2 px-4 rounded-md">Nieuwe bestelling</a>
    </div>


@endsection
