@extends('page')
@section('main')
    <x-title>Er liep iets fout!</x-title>

    <div class="mb-2">
        <p>We hebben de betaling niet ontvangen.</p>
    </div>

    <div class="py-4 flex items-center justify-center">
        <a href="?retry=1" class="bg-blue-400 hover:bg-green-400 text-white py-2 px-4 rounded-md">Opnieuw proberen</a>
    </div>


@endsection
