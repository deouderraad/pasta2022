<!DOCTYPE html>

<html lang="nl">
<head>
    <title>Pasta in uw kot</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class="flex flex-col min-h-screen max-h-screen bg-gray-100">
<header class=" p-4 flex justify-between bg-white mb-2 items-center">
    <a href="{{ route('home') }}" class="block flex items-center">
        <img src="{{ asset('img/logo.png') }}" style="height: 80px" class="hidden md:block">
        <h1 class="text-3xl text-blue-500">Pasta in uw kot</h1>
    </a>

    <a href="http://deouderraad.be" class="text-blue-500 hover:text-green-400 block px-2">Ouderraad De Regenboog
        VZW</a>
    </div>
</header>

<main class="flex-grow py-4 md:px-4 justify-center items-start flex">
    <div class="p-4 bg-white rounded xl:max-w-[960px] w-full">
        @yield('main')
    </div>

</main>

<footer class="flex items-center justify-center px-4 py-2 bg-gray-300">
    <a href="{{ route('terms') }}"
       class="text-white hover:text-green-100 text-sm">Algemene voorwaarden &amp; privacy</a>
</footer>
</body>
</html>
