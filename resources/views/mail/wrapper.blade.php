<html>
<head>
    <style>
        em {
            font-weight: 600;
            font-style: normal;
        }
    </style>
</head>
<body>

<div id="wrapper" dir="ltr"
     style="margin: 0; padding: 70px 0 70px 0; -webkit-text-size-adjust: none !important; width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
    <table border="0" cellpadding="0" width="100%">
        <tr>
            <td align="center" valign="top">
                <table
                    cellpadding="10px"
                    width="600px"
                    style="border: solid 1px #4b5682;">

                    <tr>
                        <td align="left" valign="top" style="background: #4b5682;  ">
                            <h1 style='color: #ffffff;  font-size: 30px; font-weight: 300; line-height: 150%; margin: 0;'>
                                {{ $title }}</h1>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            @yield('content')
                        </td>
                    </tr>
                </table>


                <p style="font-size: 12px; color: #666">Een probleem? Meld het ons via <a
                        href="mailto:pasta@deouderraad.be">pasta@deouderraad.be</a></p>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
