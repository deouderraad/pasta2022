<?php
/**
 * @var $order \App\Models\Order
 */

?>
@extends('mail.wrapper', ['title'=>'Pasta in uw kot: afhaling'])

@section('content')
    <p>Hallo <?= $order->fname; ?>, </p>
    <p>Graag willen wij je nogmaals hartelijk bedanken voor jouw bestelling. In deze mail overlopen we even de details van de afhaling.</p>

    <p>Je koos voor een afhaling nu zaterdag 19 februari <em>tussen {{ $order->pickupLabel('en') }}</em></p>

    <p>Dank je wel voor je steun!</p>
    <p style="font-style: italic">-- Ouderraad De Regenboog VZW.</p>
    <h2 style='color: #4b5682;  font-size: 20px; font-weight: 600; line-height: 150%; '>De afhaling </h2>
    <img src="https://pasta.deouderraad.be/img/afhaling.jpg">
    <ol>
        <li>Je meldt je aan bij de eerste vrijwilliger aan de hoek van de Beekstraat / Sint-Amandstraat</li>
        <li>Je volgt de straat uiterst rechts (zodat voorbijgaand verkeer makkelijk kan voorbij rijden)</li>
        <li>Aan de busstrook overhandigen wij je het pasta-pakket. Je geeft zelf aan of we het op de achterbank of in de
            koffer plaatsen.</li>
    </ol>

    <h2 style='color: #4b5682;  font-size: 20px; font-weight: 600; line-height: 150%; '>Nog enkele aandachtspunten</h2>
    <ul>
        <li>Je draagt steeds een mondmasker bij afhaling (te voet, met de fiets of in de wagen).</li>
        <li>Je blijft niet napraten met de vrijwilligers.</li>
    </ul>

    <p>Vragen? <a href="mailto:pasta@deouderraad.be">pasta@deouderraad.be</a> of 0498/75.51.28</p>
    <p>Leuke foto’s zijn steeds welkom via Sociale Media met de hashtag <a href="https://www.facebook.com/deouderraad/">#pastainuwkot</a>
        of via mail naar
        <a href="mailto:pasta@deouderraad.be">pasta@deouderraad.be</a>.</p>
    <p>Smakelijk eten!</p>
    <p style="font-style: italic">-- Ouderraad De Regenboog VZW.</p>

    <h2 style='color: #4b5682;  font-size: 20px; font-weight: 600; line-height: 150%; '>Dit heb je besteld:</h2>
    <p>Het bestelnummer is <em><?= $order->order_num; ?></em></p>
    <table width="100%" cellspacing="0" cellpadding="4px">
        <tr>
            <td><em>Aantal</em></td>
            <td><em>Product</em></td>
            <td><em>Per stuk</em></td>
            <td><em>Totaal</em></td>
        </tr>

        @foreach($order->getOrderLines()->getLines() as $line)
            <tr>
                <td>{{ $line->getAmount() }}</td>
                <td>
                    {{ $line->getName() }}
                    @if($line->hasDescription())
                        <br>{{ $line->getDescription() }}</span>
                    @endif
                </td>
                <td>{!!  \App\Data\Shop\EuroPrice::create($line->getPrice())  !!}</td>
                <td>{!!  \App\Data\Shop\EuroPrice::create($line->getTotalPrice())  !!}</td>
            </tr>
        @endforeach

        <tr style="background: #eee; font-weight: bold">
            <td colspan="3">Totaal</td>
            <td>{!!  \App\Data\Shop\EuroPrice::create($order->getOrderLines()->getTotalPrice())  !!}</td>
        </tr>
    </table>
@endsection
