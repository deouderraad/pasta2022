<?php
/**
 * @var $order \App\Models\Order
 */

?>
@extends('mail.wrapper', ['title'=>'Pasta in uw kot: uw bestelling'])

@section('content')
    <p>Hallo <?= $order->fname; ?>, </p>
    <p>We hebben je bestelling en betaling goed ontvangen.</p>
    <p>Het bestelnummer is <em><?= $order->order_num; ?></em></p>
    <p>Dank je wel voor je steun!</p>
    <p style="font-style: italic">-- Ouderraad De Regenboog VZW.</p>

    <h2 style='color: #4b5682;  font-size: 20px; font-weight: 600; line-height: 150%; '>
        Dit heb je besteld:</h2>

    <table width="100%" cellspacing="0" cellpadding="4px">
        <tr>
            <td><em>Product</em></td>
            <td><em>Aantal</em></td>
            <td><em>Per stuk</em></td>
            <td><em>Totaal</em></td>
        </tr>

        @foreach($order->getOrderLines()->getLines() as $line)
            <tr>
                <td>{{ $line->getAmount() }}</td>
                <td>
                    {{ $line->getName() }}
                    @if($line->hasDescription())
                        <br>{{ $line->getDescription() }}</span>
                    @endif
                </td>
                <td>{!!  \App\Data\Shop\EuroPrice::create($line->getPrice())  !!}</td>
                <td>{!!  \App\Data\Shop\EuroPrice::create($line->getTotalPrice())  !!}</td>
            </tr>
        @endforeach

        <tr style="background: #eee; font-weight: bold">
            <td colspan="3">Totaal</td>
            <td>{!!  \App\Data\Shop\EuroPrice::create($order->getOrderLines()->getTotalPrice())  !!}</td>
        </tr>
    </table>
    <h2 style='color: #4b5682;  font-size: 20px; font-weight: 600; line-height: 150%; '>
        Afhaling </h2>
    <p>op zaterdag 19 februari <em>tussen {{ $order->pickupLabel('en') }}</em> in de Sint-Amandsstraat in Kortenberg</p>
@endsection
