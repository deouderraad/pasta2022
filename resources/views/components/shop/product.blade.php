<div class="my-2">
    <x-subtitle>{{ $title }}</x-subtitle>
    <p class="mb-1">{{$slot}}</p>

    <?php
    $p_val = \App\Data\Shop\Cart::current()->getPack("p_{$sku}");
    $s_val = \App\Data\Shop\Cart::current()->getSauce("s_{$sku}");
    ?>
    <div class="grid md:grid-cols-2 gap-2">
        <div>
            <label for="p_{{$sku}}">Pakket (&euro; 28)</label>
            <input type="number"
                   step="1"
                   min="0"
                   class="border pl-2 border-gray-400 w-16" id="p_{{$sku}}" name="p_{{$sku}}"
                   value="{{ $p_val }}"
            >
        </div>
        <div>
            <label for="s_{{$sku}}">Saus (&euro; 15)</label>
            <input type="number"
                   step="1"
                   min="0"
                   class="border pl-2 border-gray-400  w-16" id="s_{{$sku}}" name="s_{{$sku}}"
                   value="{{ $s_val }}"
            >
        </div>
    </div>
</div>
