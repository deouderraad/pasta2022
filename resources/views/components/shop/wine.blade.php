<?php
$cart = \App\Data\Shop\Cart::current();

$val_w = session("_old_input.w_white");
if (empty($val_w)) {
    $val_w = $cart->getWine("w_white");
}

$val_r = session("_old_input.w_red");
if (empty($val_r)) {
    $val_r = $cart->getWine("w_red");
}
?>
<div class="my-2">
    <x-title>Wijnen</x-title>
    <div class="grid lg:grid-cols-2 gap-2">
        <div>
            <x-subtitle>Witte wijn: Un momento de Verdejo (2020)</x-subtitle>
            <label for="w_white" class="">Fles (&euro; 12)</label>
            <input type="number"
                   step="1"
                   min="0"
                   class="border pl-2 border-gray-400 w-16" id="w_white" name="w_white"
                   value="{{ $val_w}}"
            >
        </div>
        <div>
            <x-subtitle>Rode wijn: Un momento de Tempranillo (2020)</x-subtitle>
            <label for="w_red" class="">Fles (&euro; 12)</label>
            <input type="number"
                   step="1"
                   min="0"
                   class="border pl-2 border-gray-400  w-16" id="w_red" name="w_red"
                   value="{{  $val_r }}"
            >
        </div>
    </div>
</div>
