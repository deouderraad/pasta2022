<?php
/**
 * @var $pack string
 */
$values = \App\Data\Shop\Cart::current()->getDessertsFor($pack);
$desserts = [
    "tira" => "Tiramisu",
    "choco" => "Chocomousse",
    "panna" => "Panna cotta met coulis van rode vruchten",
];
$errorkey = "dessert.{$pack}";
?>

<div class="my-2">
    <div class="flex justify-between mb-2">
        <x-subtitle>{{ $title }}</x-subtitle>

        @error($errorkey)
        <span class="bg-red-100 text-red-800 p-1 px-2 ml-1">{{  $message }}</span>
        @enderror
    </div>

    <div class="grid grid-cols-2 md:grid-cols-4 gap-2">

        <?php for($a = 1; $a < 5;$a++): ?>
        <?php $selected = session("_old_input.dessert.{$pack}.{$a}") ?? ($values[$a] ?? null); ?>

        <select class="border pl-2 border-gray-400 p-2" name="dessert[{{$pack}}][{{$a}}]">
            <option value="">-- Dessert {{$a}} --</option>
            <?php foreach ($desserts as $k => $label): ?>
            <option value="{{$k}}"
                    @if($k === $selected) selected @endif
            >{{ $label }}</option>
            <?php endforeach; ?>
        </select>
        <?php endfor; ?>
    </div>
</div>
