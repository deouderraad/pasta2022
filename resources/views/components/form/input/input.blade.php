<div class="mb-2 flex-grow">
    <div class="flex justify-between">
        @if($label)
            <label for="{{$id}}" class="mr-2 block p-1">{{ $label }}</label>
        @endif
        @error($id)
        <span class="bg-red-100 text-red-800 p-1 px-2 ml-1">{{  $message }}</span>
        @enderror
    </div>
    <input

        {{$attributes->merge([
        'type'=>$type,
        'id'=>$id,
        'name'=>$name,
        'value'=>$value
    ])->class($class)}}
    >
</div>
