<?php
/**
 * @var $attributes \Illuminate\View\ComponentAttributeBag
 * @var $style string
 */

$styles = [
    'px-4', 'py-2', 'rounded', 'text-white'
];

switch ($style){
    case 'error':
        $styles[] = 'bg-red-400';
        $styles[] = 'hover:bg-red-500';
        break;
    case 'ok':
        $styles[] = 'bg-blue-400';
        $styles[] = 'hover:bg-blue-500';
        break;
    default:
        $styles[] = 'bg-gray-400';
        $styles[] = 'hover:bg-gray-500';
}
$attributes = $attributes->class($styles);

?>

<button type="{{ $type }}" {{$attributes}}>
    {{$slot}}
</button>
