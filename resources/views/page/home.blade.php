@extends('page')
<?php
use App\Models\Product;
?>
@section('main')
    <section>
        <p class="mb-1">Veel mensen samenbrengen is voorlopig nog steeds niet mogelijk,
            maar gezelligheid en pasta kunnen nog steeds!
            Daarom organiseren we dit jaar de 2e editie van
            "Pasta in uw kot": bestel online en kom je bestelling veilig ophalen in onze drive-in in de
            Sint-Amandsstraat in Kortenberg op
            <span class="font-bold text-green-500">zaterdag 19 februari</span></p>

        <p class="mb-1">Met de opbrengst van dit pastafestijn ondersteunt De Ouderraad zoals steeds de werking van
            de
            school.</p>
    </section>
    <section>
        <h2 class="text-2xl text-blue-800">Op het menu</h2>
        <p class="mb-1">We bieden 2 formules aan: een all-in pasta-pakket, of aparte saus.</p>
        <p class="mb-1">Een pakket (&euro; 28) bevat alles wat je nodig hebt om een pastafeestje te bouwen voor 4
            personen: 1 liter saus, 500 gram ongekookte pasta, gemalen kaas en 4 desserts naar keuze.</p>
        <p class="mb-1">Je kan ook de saus apart kopen (&euro; 15). De sauzen zijn verpakt in zakken van 1 liter en
            mogen ingevroren worden.</p>
        <div>
            <x-subtitle>De sauzen</x-subtitle>
            <ul class="mb-1">
                <li>
                    <span class="font-bold">Bolognaise</span>
                    Spaghettisaus op basis van van varken/rundgehakt en groenten.
                </li>
                <li>
                    <span class="font-bold">Vegetarisch</span>
                    Spaghettisaus op basis van sojagehakt, tomatensaus en stukjes groenten.
                </li>
                <li>
                    <span class="font-bold">Carbonara</span>
                    Romige kaassaus met spekblokjes.
                </li>
                <li>
                    <span class="font-bold">Pesto</span>
                    Vegetarische pesto saus met tomaten.
                </li>
                <li><span class="font-bold">All'arrabbiata</span>
                    Pittige tomatensaus, spekblokjes en Parmezaanse kaas.
                </li>
            </ul>
        </div>
        <div>
            <h3 class="text-xl text-green-500"><span class="text-white bg-red-400 p-1">Nieuw!</span> Vers bereide
                desserten</h3>
            <p class="mb-1">In een pakket zitten 4 desserts inbegrepen. Hierbij heb je de keuze uit:</p>
            <ul class="mb-1">
                <li>Tiramisu</li>
                <li>Chocomousse</li>
                <li>Panna cotta met coulis van rode vruchten</li>
            </ul>
            <p class="mb-1 text-bold">Alle desserts zijn vers bereid!</p>
        </div>
        <div>
            <h3 class="text-xl text-green-500">Wijn</h3>
            <p>Bij aankoop van een pasta-pakket of zak saus kan je bij ons ook terecht voor een fles aangepaste wijn
                (&euro;
                12).</p>
            <p>Als witte wijn serveren we "Un momento de Verdejo (2020)",
                de rode wijn is "Un momento de Tempranillo (2020)".</p>
        </div>
    </section>
    <section>
        <div>
            <x-subtitle>Bestellen</x-subtitle>
            @if(getenv('STATUS') === 'closed')
                <div class="py-4 flex items-center justify-center">
                    <div class="text-white bg-red-500  block p-2 px-5 rounded-md">
                        De bestelperiode is afgelopen
                    </div>
                </div>
            @else
                <p class="mb-1">Je geeft je bestelling door via deze website en betaalt meteen online via online
                    overschrijving.</p>
                <p class="mb-1">Je kan bestellen t.e.m. <span class="font-bold text-green-500">maandag 7 februari</span>
                </p>
                <div class="py-4 flex items-center justify-center">

                    <a href="{{ route('order.step_1') }}?refresh=1"
                       class="bg-blue-400 hover:bg-green-400 text-white py-2 px-4 rounded-md">Nu bestellen</a>

                </div>
            @endif

        </div>
    </section>

@endsection
