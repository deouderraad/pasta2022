@extends('page')
@section('main')
    <x-title>Algemene voorwaarden en Privacy</x-title>
    <p class="text-gray-400 text-sm italic">Laatste aanpassing: 26 december 2021</p>
    <div>
        <x-subtitle>Voorwaarden</x-subtitle>
        <p class="mb-2">"Pasta in uw kot" is een organisatie van Ouderraad De Regenboog VZW, Kloosterstraat 10 in 3070
            Kortenberg.</p>
        <p class="mb-2">Voor alle bestellingen is contante betaling vereist, een bestelling is pas geplaatst wanneer wij
            uw betaling ontvangen hebben.</p>
        <p class="mb-2">Voor vragen over de actie, over uw bestelling, of om uw bestelling te wijzigen of te annuleren,
            kan u ons contacteren via e-mail naar <a class="text-blue-500" href="mailto:webmaster@deouderraad.be">pasta@deouderraad.be</a>.
        </p>
    </div>
    <div>

        <x-subtitle>Privacy</x-subtitle>
        <p class="mb-2">Uw privacy is belangrijk voor ons. We verzamelen enkel de gegevens die nodig zijn voor het
            ordelijk verloop van de pasta-verkoop.</p>
        <p class="mb-2">We nemen de nodige technische en organisatorische maatregelen die nodig zijn om uw gegevens
            veilig en vertrouwelijk te bewaren. Uw gegevens worden onder geen enkele voorwaarde doorgegeven aan
            derden.</p>
        <p class="mb-2">We gebruiken deze gegevens ook enkel in het kader van de pasta-verkoop.</p>
        <p class="mb-2">Om inzage te krijgen in de gegevens die we van u verzameld hebben, om deze te wijzigen of te
            laten verwijderen stuurt u een mailtje naar <a class="text-blue-500" href="mailto:webmaster@deouderraad.be">webmaster@deouderraad.be</a>.
        </p>
    </div>
@endsection
