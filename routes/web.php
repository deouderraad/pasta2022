<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page.home');
})->name('home');

Route::get('/algemene-voorwaarden', function () {
    return view('page.terms');
})->name('terms');

Route::get('/order', [\App\Http\Controllers\OrderController::class, 'step_1'])->name('order.step_1');
Route::post('/order', [\App\Http\Controllers\OrderController::class, 'handle_step_1']);

Route::get('/order/options', [\App\Http\Controllers\OrderController::class, 'step_2'])->name('order.step_2');
Route::post('/order/options', [\App\Http\Controllers\OrderController::class, 'handle_step_2']);

Route::get('/order/confirm', [\App\Http\Controllers\OrderController::class, 'confirm'])->name('order.confirm');
Route::post('/order/confirm', [\App\Http\Controllers\OrderController::class, 'handle_confirm']);

#Route::post('/mollie/{id}', [\App\Http\Controllers\MollieController::class, 'receive'])->withoutMiddleware(['csrf'])->name('mollie.receive');

Route::get('/order/done/{id}', [\App\Http\Controllers\OrderController::class, 'done'])->name('done');


Route::get('preview', function(){
    $order = \App\Models\Order::where('order_num', filter_input(INPUT_GET, 'o'))->first();

    return view('mail/order_ophaal', ["order"=>$order]);
});
