<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('total', function () {
    echo \App\Models\Order::whereNotNull('confirmed_at')->count();
});

Artisan::command('total_c', function () {
    $total = \App\Models\Order::whereNotNull('confirmed_at')->get()->reduce(function ($carry, \App\Models\Order $order) {
        return $carry + $order->getOrderLines()->getTotalPrice();
    }, 0);

    echo (new \App\Data\Shop\EuroPrice($total))->__toString();
});

Artisan::command('refresh', function () {
    \App\Models\Order::where('payment_status', 'open')->get()->take(4)->map(function (\App\Models\Order $order) {
        $order->refreshPaymentStatus(false);
    });
    echo \App\Models\Order::where('payment_status', 'open')->count();
});


Artisan::command('count_dessert', function () {
    $totals = [
        "choco" => 0,
        "tira" => 0,
        "panna" => 0
    ];

    \App\Models\Order::where('payment_status', 'paid')->get()->map(function (\App\Models\Order $order) use (&$totals) {
        $lines = $order->getOrderLines()->toArray();
        foreach ($lines as $line) {
            if ($line['data'] ?? null) {

                $totals['choco'] += $line['data']['choco'] ?? 0;
                $totals['tira'] += $line['data']['tira'] ?? 0;
                $totals['panna'] += $line['data']['panna'] ?? 0;
            }
        }
    });
    print_r($totals);
});


Artisan::command('count_dessert', function () {
    $times = [];

    \App\Models\Order::where('payment_status', 'paid')->get()->map(function (\App\Models\Order $order) use (&$times) {
        if (!array_key_exists($order->pickup_slot, $times)) {
            $times[$order->pickup_slot] = 0;
        }
        $times[$order->pickup_slot] += 1;
    });
    ksort($times);
    print_r($times);
});

Artisan::command('export', function () {
    $f = fopen('export.csv', 'w+');

    $headers = [
        'order_num' => "Nummer",
        'lname' => "Achternaam",
        'fname' => "Voornaam",
        'email' => "E-mail",
        'phone' => "Telefoon",
        'pickup_slot' => "Ophaling",
        's_bolo' => "Saus Bolognaise",
        's_veggie' => "Saus Bolognaise",
        's_carbo' => "Saus Carbonara",
        's_pesto' => "Saus Pesto",
        's_arrab' => "Saus Arrabiata",
        'p_bolo' => "Pakket Bolognaise",
        'p_veggie' => "Pakket Bolognaise",
        'p_carbo' => "Pakket Carbonara",
        'p_pesto' => "Pakket Pesto",
        'p_arrab' => "Pakket Arrabiata",
        'w_red' => "Rode wijn",
        'w_white' => "Witte wijn",
        'tira' => "Tiramisu",
        'choco' => "Chocomousse",
        'panna' => "Panna cotta",
    ];
    fputcsv($f, $headers);

    $orders = \App\Models\Order::where('payment_status', 'paid')->orderBy('order_num')->get();
    $orders->map(function (\App\Models\Order $order) use ($f) {
        $data = [
            'order_num' => $order->order_num,
            'lname' => strtoupper($order->lname),
            'fname' => $order->fname,
            'email' => $order->email,
            'phone' => $order->phone,
            'pickup_slot' => $order->pickup_slot,
            's_bolo' => 0,
            's_veggie' => 0,
            's_carbo' => 0,
            's_pesto' => 0,
            's_arrab' => 0,
            'p_bolo' => 0,
            'p_veggie' => 0,
            'p_carbo' => 0,
            'p_pesto' => 0,
            'p_arrab' => 0,
            'w_red' => 0,
            'w_white' => 0,
            'tira' => 0,
            'choco' => 0,
            'panna' => 0,
        ];

        foreach ($order->getOrderLines()->getLines() as $line) {
            $data[$line->getSku()] += $line->getAmount();

            foreach ($line->getData() as $k => $v) {
                $data[$k] += $v;
            }
        }

        fputcsv($f, $data);

        return $data;

    });
    fclose($f);
});


Artisan::registerCommand(new \App\Console\Commands\PickupMail());
